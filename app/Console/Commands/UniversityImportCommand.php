<?php

namespace App\Console\Commands;

use App\Imports\UniversityImport;
use Illuminate\Console\Command;

class UniversityImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'university:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all universities in France from api url http://universities.hipolabs.com/search?country=France';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Import started');

        $import = new UniversityImport();

        $import->import();

        foreach ($import->errorLog as $logMsg) {
            $this->error($logMsg);
        }

        foreach ($import->log as $logMsg) {
            $this->info($logMsg);
        }

        $this->info('Import ended');

        return Command::SUCCESS;
    }
}
