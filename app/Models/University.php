<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class University extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'country', 'alpha_two_code', 'state_province'];

    /**
    * Get the domains for the university.
    */
    public function domains()
    {
        return $this->hasMany(Domain::class);
    }

    /**
    * Get the web pages for the university.
    */
    public function webPages()
    {
        return $this->hasMany(WebPage::class);
    }
}
