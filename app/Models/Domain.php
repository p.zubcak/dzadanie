<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'university_id'];

    /**
    * Get the university that owns the domain.
    */
    public function university()
    {
        return $this->belongsTo(University::class);
    }
}
