<?php

namespace App\Imports;

use App\Models\Domain;
use App\Models\WebPage;
use App\Models\University;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Client\ConnectionException;


class UniversityImport
{
    const UNIVERSITIES_API_URL = 'http://universities.hipolabs.com/search?country=France';
    public $errorLog = [];
    public $log = [];
    private $processedUniversities = 0;
    private $processedDomains = 0;
    private $processedWebPages = 0;

    /**
     * Create a new UniversityImport instance.
     *
     */
    public function __construct()
    {

    }

    /**
     * This function get data from public api which is available on UNIVERSITIES_API_URL
     * and save this data to database.
     *
     * @return void
     */
    public function import(): void
    {

        try {

            $response = Http::get(self::UNIVERSITIES_API_URL);

            if ($response->successful()) {

                $result = $response->json();

                if (is_array($result) && count($result) > 0) {
                    foreach ($result as $uniData) {

                        $validator = Validator::make($uniData, [
                            'name'              => 'required|max:255',
                            'alpha_two_code'    => 'required|max:2',
                            'country'           => 'required|max:100',
                        ]);

                        if (!$validator->fails()) {

                            $university = University::updateOrCreate(
                                $validator->validated()
                            );

                            $this->processedUniversities++;

                            if (is_array($uniData['domains'])) {
                                foreach($uniData['domains'] as $domain) {

                                    $domainValidator = Validator::make(['university_id' => $university->id, 'name' => $domain], [
                                        'university_id'     => 'required',
                                        'name'              => 'required|max:255',
                                    ]);

                                    if (!$domainValidator->fails()) {
                                        Domain::updateOrCreate(
                                            $domainValidator->validated()
                                        );
                                        $this->processedDomains++;

                                    } else {
                                        $this->addErrorLog('University domain validation failed: ' . $domain);
                                    }

                                }
                            }

                            if (is_array($uniData['web_pages'])) {
                                foreach($uniData['web_pages'] as $webPage) {

                                    $webPageValidator = Validator::make([
                                        'university_id' => $university->id,
                                        'name' => $webPage
                                        ],
                                        [
                                        'university_id'     => 'required',
                                        'name'              => 'required|max:255',
                                    ]);

                                    if (!$webPageValidator->fails()) {
                                        WebPage::updateOrCreate(
                                            $webPageValidator->validated()
                                        );
                                        $this->processedWebPages++;
                                    } else {
                                        $this->addErrorLog('University web page validation failed: '. $webPage);
                                    }

                                }
                            }

                        } else {
                            $this->addErrorLog('University validation failed: '. json_encode($uniData));
                        }

                    }

                }

            } else {
                $this->addErrorLog('API request error: ' . $response->status());
            }

        } catch (ConnectionException $e) {
            $this->addErrorLog('API connection error: ' . $e->getMessage());
        }

        $this->addLog('Processed universities: ' . $this->processedUniversities );
        $this->addLog('Processed domains: ' . $this->processedDomains );
        $this->addLog('Processed web pages: ' . $this->processedWebPages );

    }

    /**
     * This function add message to import error log
     *
     * @param string $msg
     * @return void
     */
    public function addErrorLog(string $msg): void
    {
        $this->errorLog[] = $msg;
    }

    /**
     * This function add message to import log
     *
     * @param string $msg
     * @return void
     */
    public function addLog(string $msg): void
    {
        $this->log[] = $msg;
    }

}