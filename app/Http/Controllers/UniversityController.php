<?php

namespace App\Http\Controllers;

use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UniversityResource;

class UniversityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request->get('name');
        $domain = $request->get('domain');

        $universities = University::with('domains')
            ->with('webPages')
            ->when($name, function ($query, $name) {
                return $query->where('name', 'like', '%'.$name.'%');
            })
            ->when($domain, function ($query, $domain) {
                return $query->whereHas('domains', function ($query) use ($domain) {
                    $query->where('name', $domain);
                });
            })
            ->paginate(30);

        return UniversityResource::collection($universities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        $validator = Validator::make($input, [
            'name'              => 'required|max:255',
            'alpha_two_code'    => 'required|max:2',
            'country'           => 'required|max:100',
        ]);

        if($validator->fails()){
            $response = [
                'success' => false,
                'message' => $validator->errors(),
            ];

            return response()->json($response, 400);
        }

        try {

            $university = University::create($input);

            if (is_array($input['domains'])) {
                foreach ($input['domains'] as $domain) {
                    $domainsValidator = Validator::make($domain, [
                        'name'  => 'required|max:255',
                    ]);

                    if(!$domainsValidator->fails()){
                        $university->domains()->create($domain);
                    }
                }
            }

            if (is_array($input['web_pages'])) {
                foreach ($input['web_pages'] as $webPage) {
                    $webPagesValidator = Validator::make($webPage, [
                        'name'  => 'required|max:255',
                    ]);

                    if(!$webPagesValidator->fails()){
                        $university->webPages()->create($webPage);
                    }
                }
            }

            $response = [
                'success' => true,
                'data'    => new UniversityResource($university),
            ];

            return response()->json($response, 201);

        } catch(QueryException $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];

            return response()->json($response, 500);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
